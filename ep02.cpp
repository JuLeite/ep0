//EP 0: MAC 323
//Segunda tentativa

#include <bits/stdc++.h>
using namespace std;

class Aviao {
    
    private:
    string id;          // indentificação do avião
    string cod;         // identificação do aeroporto
    int t = -1;         // tempo de voo
    int c = -1;         // combustível
    bool dec;           // é decolagem?
    bool sos = false;   // é emergência?

    public:
    int espera = 0;
    Aviao* prox = nullptr;
    Aviao* ant = nullptr;
    
    //pega os dados e cria o avião
    Aviao(); 
    // olha o combustível e tempo de voo e vê se é emergência
    void isSOS (); 
    //atualiza tempo de espera, combustível, etc e aciona o isSOS
    void attAviao();  
};

void Aviao::isSOS () {

    //se está sem combustível
    if (c == 0) 
        sos = true;
    //se ele já esperou 10% do tempo de voo
    else if (0.1 * t == espera) 
        sos = true;
}

void Aviao::attAviao () {
    
    espera++;
    if (dec == false) {
      c--;  
    }
    isSOS();
}

// adiciona os k aviões que entraram em contato na fila 
Aviao* addFila (Aviao* ini, int k); 

 // atualiza combustível e tempo de espera da fila, além da situação das pistas
 // devolve o avião que pousará/decolará naquele tempo removendo-o da fila
 // se necessário, direciona aviões para outros aeroportos
Aviao* attFila (Aviao* ini, int pistas[3]);